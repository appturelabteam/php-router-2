<!DOCTYPE html>
<html>
    <head>
        <title><?php print $GLOBALS["title"]. (!isset($_GET["print"]) ? " - ". $GLOBALS["SiteName"] : ""); ?></title>
        <base href="<?php print Router::getIndex(); ?>">
    </head>
    <body>
        <?= Template::load($web_content_folder."/layout/header.php",compact("params")) ?>
        <?php if(isset($body)) { ?><?php print $body; ?><?php } ?>
        <?= Template::load($web_content_folder."/layout/footer.php",compact("params")) ?>
    </body>
</html>